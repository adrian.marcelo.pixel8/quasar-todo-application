import { ref } from "vue";

// List of tasks (Pending, Finished, and Removed tasks)
let MyTasks = ref([]);
let FinishedTasks = ref([]);
let DeletedTasks = ref([]);

export { MyTasks, FinishedTasks, DeletedTasks };
